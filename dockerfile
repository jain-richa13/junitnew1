FROM openjdk:13
WORKDIR /
COPY target/sonarscanner-maven-basic-1.0-SNAPSHOT.jar /sonarscanner-maven-basic-1.0-SNAPSHOT.jar
ADD sonarscanner-maven-basic-1.0-SNAPSHOT.jar sonarscanner-maven-basic-1.0-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","sonarscanner-maven-basic-1.0-SNAPSHOT.jar"]
